import random
import logging
import cocotb
import math


#monitoring stuff
import ROOT
from ROOT import gStyle
import sys, os
path2add = os.path.normpath(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'python')))
if (not (path2add in sys.path)) :
    sys.path.append(path2add)
from sim_monitor import sim_monitor


from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, ReadOnly, ClockCycles
#from cocotb.drivers import BitDriver
from cocotb.regression import TestFactory
#from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
#for get_sim_time
import cocotb.utils as utils
from decimal import Decimal

#TCDS triggers
import sys, os
path2add = os.path.normpath(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'LHCTriggers')))
if (not (path2add in sys.path)) :
    sys.path.append(path2add)

import LHCTriggers

#For FE/Pipeline simulation of the tags
import queue


TRIGGER_COUNT = 1000


#LHC_BX_RATE_MHZ = 40.079
LHC_BX_RATE_MHZ = 40
LHC_BX_PERIOD_NS = 1000.0/(LHC_BX_RATE_MHZ)

TCDS_CLK_MULT          = 8
TCDS_CLK_FREQUENCY_MHZ = LHC_BX_RATE_MHZ*TCDS_CLK_MULT
TCDS_CLK_PERIOD_NS     = 1000.0/TCDS_CLK_FREQUENCY_MHZ

PIPELINE_CLK_FREQUENCY_MHZ = 312.5
PIPELINE_CLK_PERIOD_NS     = 1000.0/PIPELINE_CLK_FREQUENCY_MHZ

CHIP_TAG_DELIVERY_DELAY_US = 20

DAQ_TRIGGER_INDEX  = 0
LUMI_TRIGGER_INDEX = 1


#E-Link stuff
AVERAGE_EVENT_SIZE_BITS=300.0
ELINK_GBPS=1.28


def count_set_bits(n):
    count = 0
    while n:
        n &= n - 1
        count += 1
    return count

        

class TB(object):

    def __init__(self, dut, debug=False):        
        self.dut = dut
        self.triggers_TTC_in = 0
        self.triggers_FE_out = 0
        #self.trigger_masks = [0]*16
        self.trig_mask_hist = ROOT.TH1F("Trig_mask","Trigger mask",16,0,16)
        self.FE_tags_out = 0
        self.tags_ret_in = 0
        name = "Tag FIFO ("+str(TRIGGER_COUNT)+" triggers, "+str(CHIP_TAG_DELIVERY_DELAY_US)+" #mus hold)"
        self.FIFO_hist = ROOT.TH1F("FIFO_avail_hist",name,55,0,55)
        self.fifo_occ = 54

        # Set verbosity on our various interfaces
        level = logging.DEBUG if debug else logging.WARNING
        self.FEPipelineFIFO = queue.Queue()
        
    
    async def reset(self, duration=10000):
        self.dut.log.debug("TCDS reset")
        self.dut.TCDS_reset <= 1
        await Timer(duration,units='ns')
        await RisingEdge(self.dut.TCDS_clk)
        self.dut.TCDS_reset <= 0
        self.dut.log.debug("TCDS reset Finished")


    async def triggers(self, trigger_rate = 750):
        #create trigger generator
        trigGen = LHCTriggers.LHCTriggers()
        trigGen.Setup(trigger_rate,int(LHC_BX_RATE_MHZ*1000))  #750kHz triggers on 40Mhz BX rate
        trigGen.AddTriggerRule(8,130) #no more than 8 triggers in 130 BX
        self.dut.TTC_trigger_ID <= 0
        self.dut.TTC_Trigger_type[DAQ_TRIGGER_INDEX]  <= 0
        self.dut.TTC_Trigger_type[LUMI_TRIGGER_INDEX] <= 0
        self.dut.TTC_Trigger_dv <= 0

        while True:
            #Wait for next BX
            await ClockCycles(self.dut.TCDS_clk,1) 
            #de-assert trigger valid
            self.dut.TTC_Trigger_dv <= 0
            await ClockCycles(self.dut.TCDS_clk,
                              TCDS_CLK_MULT-1) 

            #current BX
            if self.dut.TCDS_reset.value == 0:
                self.dut.TTC_trigger_ID <= self.dut.TTC_trigger_ID.value + 1
                self.dut.TTC_trigger_type[DAQ_TRIGGER_INDEX] <= trigGen.GetTrigger()
                self.dut.TTC_Trigger_type[LUMI_TRIGGER_INDEX] <= 0
                self.dut.TTC_Trigger_dv <= 1
                self.triggers_TTC_in = self.triggers_TTC_in + self.dut.TTC_trigger_type[DAQ_TRIGGER_INDEX].value



    async def FrontEndProcessing(self):
        outFileTrig = open("FETriggers_stim.dat","w");
        outFileData = open("FEDataDelivery_stim.dat","w");
        while True:
            await FallingEdge(self.dut.TCDS_reset)
            while self.dut.TCDS_reset.value == 0:
                await ClockCycles(self.dut.TCDS_clk,1) 
                if self.dut.FE_trigger_out_dv.value == 1:
                    #decrement the FIFO occupancy
                    self.fifo_occ = self.fifo_occ-1

                    #histogram the mask values
                    mask_value = int(self.dut.FE_trigger_out.tag_BX_mask.value)
                    self.trig_mask_hist.Fill(int(mask_value))
                    self.triggers_FE_out = self.triggers_FE_out + count_set_bits(mask_value)
                    self.FE_tags_out = self.FE_tags_out + 1
                    
                    #get current time
                    current_time = utils.get_sim_time(units='ns')
                    
                    #pass on the delivery time to the local FIFO (ns)
                    delivery_time = current_time + (CHIP_TAG_DELIVERY_DELAY_US*1000)

                    #Loop over each event that was in the mask sent with this tag
                    for i in range(0,4):
                        if mask_value & (1<<i):
                            #if there was a trigger, wait for the time it would take to transfer it  
                            delivery_time  = delivery_time + AVERAGE_EVENT_SIZE_BITS/(ELINK_GBPS)
                            #add FEData stimulation
                            outFileData.write("%f 0x%02X 0x%01X\n" % 
                                              (delivery_time,
                                               int(self.dut.FE_trigger_out.tag.value),
                                               i));                            
                    #we've sent all the masked BXs, return the tag
                    self.FEPipelineFIFO.put((delivery_time,self.dut.FE_trigger_out.tag.value))

                    #add FEtrigger stimulation
                    outFileTrig.write("%f 0x%02X 0x%01X\n" % (current_time,
                                                              int(self.dut.FE_trigger_out.tag.value),
                                                              int(mask_value)));

        outFileTrig.close()
        outFileData.close()

    
    async def TagReturn(self):
        self.dut.returned_tag <= 63
        self.dut.returned_tag_dv <= 0
        self.dut.downstream_ready <= 1 # fix this
        while True:
            await ClockCycles(self.dut.Pipeline_clk,1)
            self.dut.returned_tag_dv <= 0
            if not self.FEPipelineFIFO.empty():
                delivery_time,returned_tag = self.FEPipelineFIFO.get()
                current_time = utils.get_sim_time(units='ns')
                if (delivery_time-current_time > 0):
                    wait_steps = utils.get_sim_steps(math.floor(delivery_time-current_time),units='ns')
                    await Timer(wait_steps,units='step')                    
                #wait for the next clock after delivery time
                await RisingEdge(self.dut.TCDS_clk)
                self.dut.returned_tag <= returned_tag
                self.dut.returned_tag_dv <= 1
                self.tags_ret_in = self.tags_ret_in + 1
                self.fifo_occ = self.fifo_occ+1
    def MonitorMessage(self):
        return "Trigger % 6d of % 6d" %(self.triggers_TTC_in, TRIGGER_COUNT)

async def run_test(dut):

    tb = TB(dut)

    monitor = sim_monitor.sim_monitor(10,tb.MonitorMessage)
    cocotb.fork(monitor.SimMonitor())


    #for simulation c-routines
    cocotb.fork(tb.triggers())
    cocotb.fork(tb.FrontEndProcessing())
    cocotb.fork(tb.TagReturn())

    #start the clocks
    cocotb.fork(Clock(dut.TCDS_clk,TCDS_CLK_PERIOD_NS ,units='ns').start())  #TCDS clock
    cocotb.fork(Clock(dut.Pipeline_clk,PIPELINE_CLK_PERIOD_NS,units='ns' ).start())  #PIPELINE clock
    #resets
    cocotb.fork(tb.reset(10*LHC_BX_PERIOD_NS))

    while tb.triggers_TTC_in < TRIGGER_COUNT:
        tb.FIFO_hist.Fill(tb.fifo_occ)
        await ClockCycles(tb.dut.Pipeline_clk,1)


    dut._log.info("Triggers from TTC: % 6d" % tb.triggers_TTC_in)
    dut._log.info("Triggers to FE:    % 6d" % tb.triggers_FE_out)
    dut._log.info("Tags out:          % 6d" % tb.FE_tags_out)
    dut._log.info("Tags returned:     % 6d" % tb.tags_ret_in)

    #############################################################################
    #ROOT stuff
    #############################################################################
    c1 = ROOT.TCanvas("c1","c1",1000,1000)

    #hist mask hist
    c1.cd().SetLogy()
    gStyle.SetOptStat("ne");
    tb.trig_mask_hist.GetXaxis().SetTitle("Pattern");
    tb.trig_mask_hist.GetYaxis().SetTitle("Count");
    tb.trig_mask_hist.Draw("")
    c1.Print("TrigMask.pdf")


    #Occupancy HIST
    c1.cd().SetLogy()
    gStyle.SetOptStat("emrou");
    tb.FIFO_hist.GetXaxis().SetTitle("Available tags (max = 54)");
    tb.FIFO_hist.GetYaxis().SetTitle("Clock ticks");
    tb.FIFO_hist.Draw("")
    c1.Print("FIFO_avail.pdf")

    outHistFile = ROOT.TFile.Open("Histograms.root","RECREATE")
    outHistFile.cd()
    tb.trig_mask_hist.Write()
    tb.FIFO_hist.Write()
    outHistFile.Close()

factory = TestFactory(run_test)
factory.generate_tests()
