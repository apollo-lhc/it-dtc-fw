import random
import logging
import cocotb
import math

#monitoring stuff
import ROOT
from ROOT import gStyle

import sys, os
path2add = os.path.normpath(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'python')))
if (not (path2add in sys.path)) :
    sys.path.append(path2add)
from sim_monitor import sim_monitor
#from ../python/sim_monitor import sim_monitor

#cocotb things
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, ReadOnly, ClockCycles, Join, Combine
#from cocotb.drivers import BitDriver
from cocotb.regression import TestFactory
#from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
#for get_sim_time
import cocotb.utils as utils
from decimal import Decimal


#LHC_BX_RATE_MHZ = 40.079
LHC_BX_RATE_MHZ = 40
LHC_BX_PERIOD_NS = 1000.0/(LHC_BX_RATE_MHZ)

TCDS_CLK_MULT          = 8
TCDS_CLK_FREQUENCY_MHZ = LHC_BX_RATE_MHZ*TCDS_CLK_MULT
TCDS_CLK_PERIOD_NS     = 1000.0/TCDS_CLK_FREQUENCY_MHZ

PIPELINE_CLK_FREQUENCY_MHZ = 312.5
PIPELINE_CLK_PERIOD_NS     = 1000.0/PIPELINE_CLK_FREQUENCY_MHZ

CHIP_TAG_DELIVERY_DELAY_US = 20

DAQ_TRIGGER_INDEX  = 0
LUMI_TRIGGER_INDEX = 1

def count_set_bits(n):
    count = 0
    while n:
        n &= n - 1
        count += 1
    return count


def CountLinesInFile(filename):
    num_lines = sum(1 for _ in open(filename))
    return num_lines

class TB(object):

    def __init__(self, dut, debug=False):        
        #sim variables
        self.dut = dut

        #error injection
        self.skip_expected_line = 80
        self.skip_datastream_line = 40

        #progress
        self.ExpectedTriggersLineCount   = 0
        self.ExpectedTriggersLineCounter = 0
        self.DatastreamLineCount   = 0
        self.DatastreamLineCounter = 0

        #histograms
        self.hist_fifo_occ = ROOT.TH1F("FIFO_occ","FIFO_occ",55,0,55)


        # Set verbosity on our various interfaces
        level = logging.DEBUG if debug else logging.WARNING
        
    
    async def reset(self, duration=10000):
        self.dut._log.debug("reset")
        self.dut.reset <= 1
        await Timer(duration,units='ns')
        await RisingEdge(self.dut.clk)
        self.dut.reset <= 0
        self.dut._log.debug("reset Finished")


    async def ExpectedTriggers(self):
        self.dut._log.debug("Expected Triggers Start\n")
        inFileName="FETriggers_stim.dat"
        self.ExpectedTriggersLineCount   = CountLinesInFile(inFileName)
        inFile = open(inFileName,"r");
        
        event_number = 1        
        for line in inFile:
            self.ExpectedTriggersLineCounter = self.ExpectedTriggersLineCounter + 1

            if self.ExpectedTriggersLineCounter == self.skip_expected_line:
                continue

            #set initial conditions
            self.dut.expected_trigger_dv <= 0


            #parse the stimulus line from the file
            line_split = line.split()
            event_time = float(line_split[0])
            tag6b      = int(line_split[1],0)
            tab_bx_mask  = int(line_split[2],0)
            
            
            #wait until we apply this event
            current_time = utils.get_sim_time(units='ns')
            if (event_time-current_time > 0):
                wait_steps = utils.get_sim_steps(math.floor(event_time-current_time),units='ns')
                await Timer(wait_steps,units='step')             
            await RisingEdge(self.dut.clk)
            self.dut._log.debug("Chip sent trigger+tag 0x%02X / 0x%01X",tag6b,tab_bx_mask)
            #apply the stimulus
            self.dut.expected_trigger.FE_trig.tag         <= tag6b
            self.dut.expected_trigger.FE_trig.tag_bx_mask <= tab_bx_mask
            self.dut.expected_trigger.bx_mask_DAQ         <= tab_bx_mask
            self.dut.expected_trigger.bx_mask_LUMI        <= 0
            self.dut.expected_trigger.partial_bx_ID       <= (event_number ^ 3) & 0xFF #remove the lower 2 bits
            event_number = event_number + 1
            self.dut.expected_trigger_dv <= 1
            await RisingEdge(self.dut.clk)
        inFile.close()

    async def Datastream(self):
        self.dut._log.debug("Datastream Start\n")
        inFileName="FEDataDelivery_stim.dat"
        self.DatastreamLineCount   = CountLinesInFile(inFileName)
        inFile = open(inFileName,"r");

        self.dut.datastream_trigger_done <= 1
        line_count =0
        for line in inFile:
            line_count = line_count + 1
            self.DatastreamLineCounter = self.DatastreamLineCounter + 1

            if line_count == self.skip_datastream_line:
                continue


            #set initial conditions
            self.dut.datastream_trigger_tag_dv <= 0

            #parse the stimulus line from the file
            line_split = line.split()
            event_time = float(line_split[0])
            tag6b      = int(line_split[1],0)
            tag_bx_ID  = int(line_split[2],0)
            
            
            #wait until we apply this event
            current_time = utils.get_sim_time(units='ns')
            if (event_time-current_time > 0):
                wait_steps = utils.get_sim_steps(math.floor(event_time-current_time),units='ns')
                await Timer(wait_steps,units='step')             
            await RisingEdge(self.dut.clk)


            self.dut._log.debug("Processing trigger tag+BX 0x%02X / 0x%01X ",tag6b,tag_bx_ID)
            
            #set the MSBs of the trigger tag from the 6b tag
            for iBit in range(0,6):
                self.dut.datastream_trigger_tag[iBit+2] <= (tag6b >> iBit) & 1
                #set the LSBs of the trigger tag from the mask bit
            for iBit in range(0,2):
                self.dut.datastream_trigger_tag[iBit] <= (tag_bx_ID >> iBit) &1
            #set dv
            self.dut.datastream_trigger_tag_dv <= 1
            #1 clock tick of data valid
            await ClockCycles(self.dut.clk,1) 
            self.dut.datastream_trigger_tag_dv <= 0

            await RisingEdge(self.dut.datastream_trigger_valid)

            #wait 10 clock ticks for data to flow
            await ClockCycles(self.dut.clk,10) 
            #say we are done for one clock tick
            self.dut.datastream_trigger_done <= 1
            await ClockCycles(self.dut.clk,1 ) 
        inFile.close()

    async def TagReturn(self):
        while True:
            await RisingEdge(self.dut.returned_tag_dv)
            self.dut._log.debug("Returned tag %d",self.dut.returned_tag.value)

    async def HistogramFiller(self):
        while True:
            await ClockCycles(self.dut.clk,1)
            if self.dut.reset.value == 0:
                value = int(self.dut.TagDataController_1.data_count)
                self.hist_fifo_occ.Fill(value)
                
    def MonitorMessage(self):
        return "ExpectedTriggers: (% 6d/% 6d)    Datastream: (% 6d/% 6d)" % (
            self.ExpectedTriggersLineCounter,
            self.ExpectedTriggersLineCount,
            self.DatastreamLineCounter,
            self.DatastreamLineCount)

            
async def run_test(dut):

    tb = TB(dut,True)

#    monitor = sim_monitor(10,tb.MonitorMessage)
    monitor = sim_monitor.sim_monitor(10,tb.MonitorMessage)
    cocotb.fork(monitor.SimMonitor())


    print("start")
    tb.dut._log.debug("Start")

    #for simulation c-routines
    Exp_trig_task   = cocotb.fork(tb.ExpectedTriggers())
    Datastream_task = cocotb.fork(tb.Datastream())
    cocotb.fork(tb.TagReturn())
    cocotb.fork(tb.HistogramFiller())


    #start the clocks
    cocotb.fork(Clock(dut.clk,TCDS_CLK_PERIOD_NS ,units='ns').start())  #TCDS clock
    #resets
    cocotb.fork(tb.reset(10*LHC_BX_PERIOD_NS))
    

    #wait for stimulus to end
    await Combine(Join(Exp_trig_task),
                  Join(Datastream_task))

    
    print(tb.dut.counter[0].value)
    print(tb.dut.counter[1].value)
    print(tb.dut.counter[2].value)
    print(tb.dut.counter[3].value)
    print(tb.dut.counter[4].value)
    print(tb.dut.counter[5].value)

    #############################################################################
    #ROOT stuff
    #############################################################################
    c1 = ROOT.TCanvas("c1","c1",1000,1000)
    #Occupancy HIST
    c1.cd().SetLogy()
    gStyle.SetOptStat("emrou");
    tb.hist_fifo_occ.GetXaxis().SetTitle("Occupancy");
    tb.hist_fifo_occ.GetYaxis().SetTitle("Clock ticks");
    tb.hist_fifo_occ.Draw("")
    c1.Print("FIFO_occ.pdf")
    
    outHistFile = ROOT.TFile.Open("Histograms.root","RECREATE")
    outHistFile.cd()
    tb.hist_fifo_occ.Write()
    outHistFile.Close()

factory = TestFactory(run_test)
factory.generate_tests()
