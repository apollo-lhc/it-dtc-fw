# IT-DTC Firmware 
This is a repository for IT-DTC firmware and is intended to be used as a submodule in other builds.

The building of ipcores and axi slaves is done via the build system located here: https://github.com/apollo-lhc/CM_FPGA_FW