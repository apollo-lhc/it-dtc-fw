library ieee;
use ieee.std_logic_1164.all;

use work.EventBuilder_PKG.all;
use work.FragmentBuilder_PKG.all;

entity FragmentBuilder_StateMachine is
    port (
    clk_PL               : in  std_logic;
    reset_PL             : in  std_logic;    
    state_o              : out state_t;
    meta_words_left_i    : in  integer range 0 to FRAGMENT_META_WORD_COUNT-1;
    fragment_start_i     : in  std_logic;
    fragment_size_left_i : in unsigned(15 downto 0)
    );
end entity FragmentBuilder_StateMachine;

architecture behavioral of FragmentBuilder_StateMachine is
begin
  state_machine_proc: process (clk_PL, reset_PL) is
  begin  -- process state_machine_proc
    if reset_PL = '1' then              -- asynchronous reset (active high)
      state <= SM_RESET;
    elsif clk_PL'event and clk_PL = '1' then  -- rising clock edge
      case state is
        when SM_RESET =>
          ---SM_RESET------------------------------------------------------------
          state <= SM_INIT;
        when SM_INIT  =>
          ---SM_INIT-------------------------------------------------------------
          state <= SM_WAITING;
        when SM_WAITING =>
          ---SM_WAITING----------------------------------------------------------
          if meta_words_left = 0 then
            state <= SM_READY;
          else
            state <= SM_WAITING;
          end if;
        when SM_READY =>
          ---SM_READY------------------------------------------------------------
          if fragment_start = '1' then
            --start streaming the data
            state <= SM_STREAMING;
          else
            state <= SM_READY;
          end if;          
        when SM_STREAMING =>
          ---SM_STREAMING--------------------------------------------------------
          if fragment_size_left = 0 then
            state <= SM_WAITING;
          else
            state <= SM_STREAMING;
          end if;
          
        when SM_ERROR =>
          ---SM_ERROR------------------------------------------------------------
          state <= SM_RESET;
        when others =>
          state <= SM_ERROR;
      end case;
    end if;
  end process state_machine_proc;

end architecture behavioral;
