library ieee;
use ieee.std_logic_1164.all;

use work.EventBuilder_PKG.all;

entity FragmentBuilder is
  
  port (
    clk_PL          : in  std_logic;
    reset_PL        : in  std_logic;
    chip_data       : in  std_logic_vector(CHIP_DATA_WIDTH -1 downto 0);
    chip_data_valid : in  std_logic;
    chip_data_ack   : out std_logic;
    
    chip_meta       : in  std_logic_vector(CHIP_META_WIDTH -1 downto 0);
    chip_meta_valid : in  std_logic;
    chip_meta_ack   : out std_logic;

    clk_DAQ   : in std_logic;
    reset_DAQ : in std_logic;
    fragment_ready : out std_logic;
    fragment_ID    : out std_logic_vector(BX_ID_WIDTH -1 downto 0);
    fragment_size  : out std_logic_vector(CHIP_SIZE_WIDTH -1 downto 0);
    fragment_start : in  std_logic;
    );

end entity FragmentBuilder;

architecture structural of FragmentBuilder is
  -------------------------------------------------------------------------------
  -- Overall signals
  -------------------------------------------------------------------------------
  signal state : state_t;

  -------------------------------------------------------------------------------
  -- fragment fifo interface
  -------------------------------------------------------------------------------
  signal meta_words_left    : integer range 0 to FRAGMENT_META_WORD_COUNT-1;
  signal FM_error_pulse     : std_logic_vector(1 downto 0);
  constant FM_ERROR_INCORRECT_START,FM_ERROR_BAD_START : integer := (0,1);
  signal fragment_ID        : std_logic_vector(BX_ID_WIDTH-1 downto 0);
  signal fragment_size_left : unsigned(15 downto 0);
  signal fragment_start_bit : std_logic_vector(5 downto 0);
  signal fragment_stop_bit  : std_logic_vector(5 downto 0);
  
  -------------------------------------------------------------------------------
  -- DAQ interface
  -------------------------------------------------------------------------------
begin  -- architecture behavioral

  --State machine controller
  FragmentBuilder_StateMachine_1: entity work.FragmentBuilder_StateMachine
    port map (
      clk_PL               => clk_PL,
      reset_PL             => reset_PL,
      state_o              => state,
      meta_words_left_i    => meta_words_left,
      fragment_start_i     => fragment_start,
      fragment_size_left_i => fragment_size_left);


  --Interaction with the fragment FIFO (data source)
  FragmentBuilder_fragment_FIFO_intf_1: entity work.FragmentBuilder_fragment_FIFO_intf
    port map (
      clk_PL               => clk_PL,
      reset_PL             => reset_PL,
      state_i              => state,
      chip_meta_i          => chip_meta,
      chip_meta_valid_i    => chip_meta_valid,
      chip_meta_ack_o      => chip_meta_ack,
      meta_words_left_o    => meta_words_left,
      fragment_ID_o        => fragment_ID,
      fragment_size_left_o => fragment_size_left,
      fragment_start_bit_o => fragment_start_bit,
      fragment_stop_bit_o  => fragment_stop_bit);

  --Fragment data alignment for DAQ
  
  
  --Interaction with the DAQ Event builder (data sink)
  
  
end architecture behavioral;
