library ieee;
use ieee.std_logic_1164.all;

use work.EventBuilder_PKG.all;

entity FragmentBuilder_fragment_FIFO_intf is
  
  port (
    clk_PL               : in  std_logic;
    reset_PL             : in  std_logic;
    state_i              : in  state_t;
    chip_meta_i          : in  chip_meta_array_t(CHIP_COUNT downto 1);
    chip_meta_valid_i    : in  std_logic_vector(CHIP_COUNT downto 1);
    chip_meta_ack_o      : out std_logic_vector(CHIP_COUNT downto 1);
    meta_words_left_o    : out integer range 0 to FRAGMENT_META_WORD_COUNT-1;
    fragment_ID_o  : out std_logic_vector(BX_ID_WIDTH-1 downto 0);
    fragment_size_o : out unsigned(15 downto 0);                   
    fragment_start_bit_o : out std_logic_vector(5 downto 0);            
    fragment_stop_bit_o  : out std_logic_vector(5 downto 0)
    );
end entity FragmentBuilder_fragment_FIFO_intf;

architecture behavioral of FragmentBuilder_fragment_FIFO_intf is

  signal error_pulse : std_logic_vector(1 downto 0);
  signal error_count : uint32_array_t(error_pulse'RANGE);
  
  -------------------------------------------------------------------------------
  -- fragment fifo interface
  -------------------------------------------------------------------------------
  signal meta_words_left : integer range 0 to FRAGMENT_META_WORD_COUNT-1;

  signal fragment_size : unsigned(15 downto 0);
  signal fragment_start_bit : std_logic_vector(5 downto 0);
  signal fragment_stop_bit : std_logic_vector(5 downto 0);

  --Monitoring
  signal FM_error_pulse : std_logic_vector(1 downto 0);
  constant FM_ERROR_INCORRECT_START,FM_ERROR_BAD_START : integer := (0,1);

begin  -- architecture behavioral

  meta_words_left_o <= meta_words_left;
  
  --ack the meta data when it is valid and we are in the SM_WAITING state
  chip_meta_ack <= '1' when (chip_meta_valid = '1' and
                             state = SM_WAITING) else
                   '0';
  fragment_FIFO_intf: process (clk_PL, reset_PL) is
  begin  -- process fragment_FIFO_intf
    if reset_PL = '1' then              -- asynchronous reset (active high)
      
    elsif clk_PL'event and clk_PL = '1' then  -- rising clock edge
      fragment_read_o <= '0';
      FM_error_pulse <= ( others => '0');
      
      case state is
        when SM_WAITING =>
          ---SM_WAITING----------------------------------------------------------
          if chip_meta_valid = '1' then

            --Move through the meta words
            if meta_words_left /= 0 then
              meta_words_left <= meta_words_left -1;
            end if;
            
            --Process each word and buffer the contents.            
            case meta_words_left is
              when FM_BX_WORD        =>
                fragment_ID_o <= chip_meta(7 downto 0);
              when FM_START_BIT_WORD =>
                fragment_start_bit <= chip_meta(5 downto 0);
              when FM_SIZE_MSB_WORD  =>
                fragment_size(15 downto  8) <= chip_meta(7 downto 0);
              when FM_SIZE_LSB_WORD  =>
                fragment_size( 7 downto  0) <= chip_meta(7 downto 0);
              when FM_STOP_BIT_WORD =>
                fragment_stop_bit <= chip_meta(5 downto 0);
              when others => null;
            end case;

            
            --Check if we have any errors from the meta word start bit
            case meta_words_left is
              when FM_BX_WORD        =>
                FM_error_pulse(FM_ERROR_BAD_START) <= not chip_meta(8);
                --keep waiting for a correct start bit
                meta_words_left <= FRAGMENT_META_WORD_COUNT-1;
              when FM_START_BIT_WORD | FM_SIZE_MSB_WORD |FM_SIZE_LSB_WORD | FM_STOP_BIT_WORD =>
                FM_error_pulse(FM_ERROR_INCORRECT_START) <= chip_meta(8);
              when others => null;
            end case;
            
                        
          end if;
        when SM_READY =>
          ---SM_READY------------------------------------------------------------
          fragment_ready_o <= '1';
          
        when SM_STREAMING =>
          ---SM_STREAMING--------------------------------------------------------
        when others =>
          meta_words_left <= FRAGMENT_META_WORD_COUNT-1;
      end case;
    end if;
  end process fragment_FIFO_intf;


  error_pulse(0) <= FM_error_pulse(0);
  error_pulse(1) <= FM_error_pulse(1);
  counters: for iCNT in error_pulse'RANGE generate
    counter_1: entity work.counter
      generic map (
        roll_over   => '0')
      port map (
        clk         => clk_PL,
        reset_async => reset,
        reset_sync  => '0',
        enable      => '1',
        event       => error_pulse(iCNT),
        count       => error_count(iCNT),
        at_max      => open);    
  end generate counters;

end architecture behavioral;
    
