library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.RD53_Params_PKG.all;
use work.Trigger_PKG.all;

package TriggerMap_PKG is
  
  -------------------------------------------------------------------------------
  -- tag data storage types
  -------------------------------------------------------------------------------
  type tag_data_t is record
                                                       --BX ID skips lowest 2
                                                       --bits because of tag mask
                       partial_BX_id  : std_logic_vector(FE_BX_ID_SIZE-1+2 downto 2); 
                       tag            : Tag_t;
                       mask_DAQ       : std_logic_vector(3 downto 0);
                       mask_LUMI      : std_logic_vector(3 downto 0);
                     end record tag_data_t;
  constant TAG_DATA_T_DEFAULT : tag_data_t := (partial_bx_id  => (others => '0'),
                                               tag            => (others => '0'),
                                               mask_DAQ       => (others => '0'),
                                               mask_LUMI      => (others => '0')
                                               );
  constant TAG_DATA_T_SIZE    : integer := TAG_DATA_T_DEFAULT.partial_BX_id'LENGTH +
                                           TAG_DATA_T_DEFAULT.tag'LENGTH +
                                           TAG_DATA_T_DEFAULT.mask_DAQ'LENGTH +
                                           TAG_DATA_T_DEFAULT.mask_LUMI'LENGTH;
                                           
  function tag_data2slv (r : tag_data_t) return std_logic_vector;

  function slv2tag_data (slv : std_logic_vector) return tag_data_t;
      
      
  -------------------------------------------------------------------------------
  -- TriggerMap state machine
  -------------------------------------------------------------------------------
  type SMTM_state_t is (SMTM_RESET,
                        SMTM_INIT,
                        SMTM_IDLE,
                        SMTM_PROCESSING,
                        SMTM_STREAM,
                        SMTM_DATA_MISMATCH,
                        SMTM_ERROR);

  
end package TriggerMap_PKG;


package body TriggerMap_PKG is

    function tag_data2slv (r : tag_data_t) return std_logic_vector is
    variable slv : std_logic_vector(TAG_DATA_T_SIZE-1 downto 0);
    variable iBit : integer;
    begin
      iBit := 0;

      slv(r.partial_BX_id'RANGE)                     := r.partial_BX_id;
      iBit := iBit+r.partial_BX_id'LENGTH;

      slv(r.tag'HIGH + iBit downto iBit)       := std_logic_vector(r.tag);
      iBit := iBit+r.tag'LENGTH;

      slv(r.mask_DAQ'HIGH + iBit downto iBit)  := r.mask_DAQ;
      iBit := iBit+r.mask_DAQ'LENGTH;
      
      slv(r.mask_LUMI'HIGH + iBit downto iBit) := r.mask_LUMI;
      iBit := iBit+r.mask_LUMI'LENGTH;

      return  slv;
    end;
  function slv2tag_data (slv : std_logic_vector) return tag_data_t is
    variable r : tag_data_t;
    variable iBit : integer;
    begin
      iBit := 0;

      r.partial_BX_id   := slv(r.partial_BX_id'RANGE);
      iBit := iBit+r.partial_BX_id'LENGTH;

      r.tag       := unsigned(slv(r.tag'HIGH + iBit downto iBit));
      iBit := iBit+r.tag'LENGTH;

      r.mask_DAQ  := slv(r.mask_DAQ'HIGH + iBit downto iBit);
      iBit := iBit+r.mask_DAQ'LENGTH;
      
      r.mask_LUMI := slv(r.mask_LUMI'HIGH + iBit downto iBit);
      iBit := iBit+r.mask_LUMI'LENGTH;

      return  r;
    end;
end package body TriggerMap_PKG;
