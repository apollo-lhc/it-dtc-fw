library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.RD53_Params_PKG.all;
use work.TriggerMap_PKG.all;

library xpm;
use xpm.vcomponents.all;

entity TagDataController is
  port (
    clk   : in std_logic;

    --overall TriggerMap state
    state_SMTM_i   : in SMTM_state_t;

    ready          : out std_logic;
    
    --next fragments to expect
    next_fragment_info_o        : out tag_data_t;
    next_fragment_info_dv_o     : out std_logic;
    next_fragment_info_ack_i    : in  std_logic;

    --new expected tag info 
    expected_fragment_info_i    : in  tag_data_t;
    expected_fragment_info_dv_i : in  std_logic
    
    );
end entity TagDataController;

architecture behavioral of TagDataController is
  signal tag_data_fifo_out : std_logic_vector(TAG_DATA_T_SIZE-1 downto 0);

  signal busy_expected : std_logic;
  signal busy_next_fragment : std_logic;

  signal reset : std_logic;

  signal data_count : std_logic_vector(TAG_WIDTH-1 downto 0);
begin  -- architecture behavioral

  ready <= busy_expected nor busy_next_fragment;
  reset <= '1' when state_SMTM_i =SMTM_RESET else '0';
  
  next_fragment_info_o <= slv2tag_data(tag_data_fifo_out);
  DataProcessingFIFO: xpm_fifo_async
    generic map (
      FIFO_MEMORY_TYPE => "auto", -- String
      FIFO_WRITE_DEPTH => 512, -- DECIMAL
      PROG_EMPTY_THRESH => 5, -- DECIMAL
      PROG_FULL_THRESH => 5, -- DECIMAL
      READ_DATA_WIDTH => TAG_DATA_T_SIZE, -- DECIMAL
      READ_MODE => "fwft", -- String
      RELATED_CLOCKS => 0, -- DECIMAL
      USE_ADV_FEATURES => "1404", -- String
      WRITE_DATA_WIDTH => TAG_DATA_T_SIZE, -- DECIMAL
      RD_DATA_COUNT_WIDTH => TAG_WIDTH -- DECIMAL
      )
    port map (
      rst           => reset,

      wr_clk        => clk,
      wr_rst_busy   => busy_expected,
      full          => open,
      din           => tag_data2slv(expected_fragment_info_i), 
      wr_en         => expected_fragment_info_dv_i,
      
      rd_clk        => clk,
      rd_rst_busy   => busy_next_fragment,
      empty         => open,

      dout          => tag_data_fifo_out,
      rd_en         => next_fragment_info_ack_i,
      data_valid    => next_fragment_info_dv_o,
      
      rd_data_count => data_count,

      sleep         => '0',
      injectdbiterr => '0',
      injectsbiterr => '0'
      );
  

  


end architecture behavioral;
  
