library ieee;
use ieee.std_logic_1164.all;

package FragmentBuilder_PKG is

  subtype meta_data_t is std_logic_vector(8 downto 0);
  constant FRAGMENT_META_WORD_COUNT : integer := 5;
  constant FM_BX_WORD        : integer range 0 to FRAGMENT_META_WORD_COUNT-1 := 4;
  constant FM_START_BIT_WORD : integer range 0 to FRAGMENT_META_WORD_COUNT-1 := 3;
  constant FM_STOP_BIT_WORD  : integer range 0 to FRAGMENT_META_WORD_COUNT-1 := 2;
  constant FM_SIZE_MSB_WORD  : integer range 0 to FRAGMENT_META_WORD_COUNT-1 := 1;
  constant FM_SIZE_LSB_WORD  : integer range 0 to FRAGMENT_META_WORD_COUNT-1 := 0;


  

end package FragmentBuilder_PKG;
