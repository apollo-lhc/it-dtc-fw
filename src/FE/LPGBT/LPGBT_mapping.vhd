library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.LPGBT_GENERIC_PKG.all;
use work.LPGBT_CTRL_PKG.all;
use work.LPGBT_IO_PKG.all;
use work.LPGBT_MGT_PKG.all;

entity LPGBT_mapping is
  generic (
    LPGBT_GENERIC : lpgbt_generic_array_t);
  port (
    clk_320             : in  std_logic;
    clk_40en            : in  std_logic; --clk en for 40.XMhz on the 320 clock
    locked_320          : in  std_logic; --clk locked for 320 clock
    LPGBT_MGT_up_in     : in  LPGBT_MGT_in_upstream_array_t(0 to LPGBT_GENERIC'length-1);
    LPGBT_MGT_up_out    : out LPGBT_MGT_out_upstream_array_t(0 to LPGBT_GENERIC'length-1);   
    LPGBT_MGT_down_in   : in  LPGBT_MGT_in_downstream_array_t(0 to LPGBT_GENERIC'length-1);
    LPGBT_MGT_down_out  : out LPGBT_MGT_out_downstream_array_t(0 to LPGBT_GENERIC'length-1);

    --LPGBT contrl/mon
    LPGBT_CTRL : in  LPGBT_Ctrl_array_t;
    LPGBT_MON  : out LPGBT_Mon_array_t;
    --Elink data links in/out
    LPGBT_in   : in  LPGBT_in_array_t; 
    LPGBT_out  : out LPGBT_out_array_t
    );
end entity LPGBT_mapping;

architecture structure of LPGBT_mapping is
  signal lpgbt_downlink_nrst : std_logic_vector(0 to LPGBT_GENERIC'LENGTH-1);
  signal lpgbt_uplink_nrst   : std_logic_vector(0 to LPGBT_GENERIC'LENGTH-1);

  
begin


  LPGBTs: for iLPGBT in 0 to LPGBT_GENERIC'LENGTH -1 generate
  begin
    --reset for this lpgbt fpga block from local clock and MGTs
    lpgbt_downlink_nrst(iLPGBT) <= (locked_320) and
                                    (LPGBT_MGT_down_in(iLPGBT).ready);
    lpgbt_uplink_nrst(iLPGBT)   <= (locked_320) and
                                    (LPGBT_MGT_up_in(iLPGBT).ready);

    LPGBT_Mon(iLPGBT).downlink.reset_n <= lpgbt_downlink_nrst(iLPGBT);
    LPGBT_Mon(iLPGBT).uplink.reset_n   <= lpgbt_uplink_nrst(iLPGBT);


    assert false    
      report "Downlink " & integer'image(iLPGBT) &  "\n         " & integer'image(LPGBT_GENERIC(iLPGBT).DOWN.MULTICYCLE_DELAY) & "\n         " & integer'image(LPGBT_GENERIC(iLPGBT).DOWN.CLOCK_RATIO) & "\n         " & integer'image(LPGBT_GENERIC(iLPGBT).DOWN.WIDTH) 
    severity note;
    
 
    lpgbtfpga_downlink_1: entity work.lpgbtfpga_downlink
      generic map (
        c_multicyleDelay => LPGBT_GENERIC(iLPGBT).DOWN.MULTICYCLE_DELAY,
        c_clockRatio     => LPGBT_GENERIC(iLPGBT).DOWN.CLOCK_RATIO,
        c_outputWidth    => LPGBT_GENERIC(iLPGBT).DOWN.WIDTH)
      port map (
        clk_i               => clk_320,
        clkEn_i             => clk_40en,
        rst_n_i             => lpgbt_downlink_nrst(iLPGBT),
        userData_i          => LPGBT_in(iLPGBT).user_data,
        ECData_i            => LPGBT_in(iLPGBT).ec_data,
        ICData_i            => LPGBT_in(iLPGBT).ic_data,
        mgt_word_o          => LPGBT_MGT_down_out(iLPGBT).data,
        interleaverBypass_i => LPGBT_CTRL(iLPGBT).downlink.bypass_interleaver,
        encoderBypass_i     => LPGBT_CTRL(iLPGBT).downlink.bypass_encoder,
        scramblerBypass_i   => LPGBT_CTRL(iLPGBT).downlink.bypass_scrambler,
        rdy_o               => LPGBT_Mon(iLPGBT).downlink.ready);


    
    lpgbtfpga_uplink_1: entity work.lpgbtfpga_uplink
      generic map (
        DATARATE                  => LPGBT_GENERIC(iLPGBT).UP.DATARATE,                     
        FEC                       => LPGBT_GENERIC(iLPGBT).UP.FEC,
        c_multicyleDelay          => LPGBT_GENERIC(iLPGBT).UP.MULTICYCLE_DELAY,
        c_clockRatio              => LPGBT_GENERIC(iLPGBT).UP.CLOCK_RATIO,
        c_mgtWordWidth            => LPGBT_GENERIC(iLPGBT).UP.WORD_WIDTH,
        c_allowedFalseHeader      => LPGBT_GENERIC(iLPGBT).UP.ALLOWED_FALSE_HEADER,
        c_allowedFalseHeaderOverN => LPGBT_GENERIC(iLPGBT).UP.ALLOWED_FALSE_HEADER_OVER_N,
        c_requiredTrueHeader      => LPGBT_GENERIC(iLPGBT).UP.REQUIRED_TRUE_HEADER,
        c_bitslip_mindly          => LPGBT_GENERIC(iLPGBT).UP.BITSLIP_MINDLY,
        c_bitslip_waitdly         => LPGBT_GENERIC(iLPGBT).UP.BITSLIP_WAIT_DELAY)
      port map (
        uplinkClk_i                => clk_320,
        uplinkClkOutEn_o           => LPGBT_out(iLPGBT).data_valid,
        uplinkRst_n_i              => lpgbt_uplink_nrst(iLPGBT),
        mgt_word_i                 => LPGBT_MGT_up_in(iLPGBT).data,
        userData_o                 => LPGBT_out(iLPGBT).user_data,
        EcData_o                   => LPGBT_out(iLPGBT).ec_data,
        IcData_o                   => LPGBT_out(iLPGBT).ic_data,
        bypassInterleaver_i        => LPGBT_Ctrl(iLPGBT).uplink.bypass_Interleaver,
        bypassFECEncoder_i         => LPGBT_Ctrl(iLPGBT).uplink.bypass_FEC_Encoder,
        bypassScrambler_i          => LPGBT_Ctrl(iLPGBT).uplink.bypass_Scrambler,
        mgt_bitslipCtrl_o          => LPGBT_MGT_up_out(iLPGBT).bitslip,
        dataCorrected_o            => LPGBT_Mon(iLPGBT).uplink.corrected_data,
        IcCorrected_o              => LPGBT_Mon(iLPGBT).uplink.corrected_ic,
        EcCorrected_o              => LPGBT_Mon(iLPGBT).uplink.corrected_ec,
        rdy_o                      => LPGBT_Mon(iLPGBT).uplink.ready,
        frameAlignerEven_o         => LPGBT_Mon(iLPGBT).uplink.frame_align_even);
  end generate LPGBTs;
end architecture structure;

