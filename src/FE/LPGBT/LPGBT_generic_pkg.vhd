library ieee;
use ieee.std_logic_1164.all;

package LPGBT_GENERIC_PKG is

  type LPGBT_generic_down_t is record
    MULTICYCLE_DELAY : integer range 0 to 7;
    CLOCK_RATIO      : integer;     
    WIDTH            : integer;     
  end record LPGBT_generic_down_t;

  type LPGBT_generic_up_t is record
    DATARATE                    : integer range 0 to 2;
    FEC                         : integer range 0 to 2;                    
    MULTICYCLE_DELAY            : integer range 0 to 7;
    CLOCK_RATIO                 : integer;
    WORD_WIDTH                  : integer;
    ALLOWED_FALSE_HEADER        : integer;
    ALLOWED_FALSE_HEADER_OVER_N : integer;
    REQUIRED_TRUE_HEADER        : integer;
    BITSLIP_MINDLY              : integer;
    BITSLIP_WAIT_DELAY          : integer;
  end record LPGBT_generic_up_t;

  type LPGBT_generic_t is record
    down : LPGBT_generic_down_t;
    up   : LPGBT_generic_up_t;
  end record LPGBT_generic_t;

  type LPGBT_generic_array_t is array (integer range <>) of LPGBT_generic_t;
  

end package LPGBT_GENERIC_PKG;
