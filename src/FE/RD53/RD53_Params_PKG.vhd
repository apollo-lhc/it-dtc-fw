library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


package RD53_Params_PKG is

  -------------------------------------------------------------------------------
  -- Tag info
  -------------------------------------------------------------------------------
  constant TAG_COUNT        : integer := 54;
  constant INVALID_TRIG_TAG : integer range 0 to TAG_COUNT := TAG_COUNT;
  constant TAG_WIDTH        : integer := integer(floor(log2(real(TAG_COUNT)))) + 1;
  subtype Tag_t is unsigned(TAG_WIDTH-1 downto 0);
  constant FIRST_VALID_TAG  : Tag_t := to_unsigned(0          , TAG_WIDTH);
  constant LAST_VALID_TAG   : Tag_t := to_unsigned(TAG_COUNT-1, TAG_WIDTH);

  -------------------------------------------------------------------------------
  -- Tag trig mask
  -------------------------------------------------------------------------------
  constant TAG_TRIG_MASK_WIDTH : integer := 4;
  subtype Tag_BX_mask_t is std_logic_vector(TAG_TRIG_MASK_WIDTH-1 downto 0);
  
end package RD53_Params_PKG;
