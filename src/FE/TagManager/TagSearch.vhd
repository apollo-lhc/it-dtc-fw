library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity TagSearch is
  
  generic (
    TAG_COUNT : integer := 64);

  port (
    tag_free_map   : in  std_logic_vector(TAG_COUNT-1 downto 0);
    free_tag       : out Tag_t;
    free_tag_valid : out std_logic);

end entity TagSearch;

architecture behavioral of TagSearch is

  signal tag_group_select     : std_logic_vector(2*(TAG_COUNT/4) -1 downto 0);
--groups of two bits
  signal tag_group_has_valid  : std_logic_vector(TAG_COUNT/4 -1 downto 0)
  
begin  -- architecture behavioral

  PriorityEncoder_1: entity work.PriorityEncoder
    generic map (
      INPUT_COUNT => TAG_COUNT)
    port map (
      input => tag_free_map,
      index => free_tag,
      valid => free_tab_validd);
  
  
end architecture behavioral;
