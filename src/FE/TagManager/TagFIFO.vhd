library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.RD53_Params_PKG.all;

entity TagFIFO is

  port (
    clk          : in  std_logic;
    reset        : in  std_logic;
    ready        : out std_logic;
    low_on_tags  : out std_logic;
    free_tag     : out Tag_t;
    free_tag_dv  : out std_logic;
    free_tag_ack : in  std_logic;
    used_tag     : in  Tag_t;
    used_tag_dv  : in  std_logic);
  
end entity TagFIFO;

architecture behavioral of TagFIFO is
  type State_t is (S_RESET,
                   S_FILL,
                   S_RUNNING,
                   S_ERROR);
  signal state : State_t;

  signal fill_tag     : unsigned(TAG_WIDTH-1 downto 0);
  signal fill_tag_dv  : std_logic;
  
  signal fifo_wr_data : std_logic_vector(TAG_WIDTH-1 downto 0);
  signal fifo_wr_dv   : std_logic;

  signal free_tag_local : std_logic_vector(TAG_WIDTH-1 downto 0);
  
  signal fifo_low     : std_logic;

  signal wr_rst_busy  : std_logic;
  signal rd_rst_busy  : std_logic;
begin  -- architecture behavioral

  ready       <= '1' when state = S_RUNNING
                 else '0';

  low_on_tags <= '1' when state = S_RUNNING and fifo_low = '1'
                 else '0';
  
  reset_init_controller: process (clk, reset) is
  begin  -- process reset_init_controller
    if reset = '1' then                 -- asynchronous reset (active high)
      state <= S_RESET;
    elsif clk'event and clk = '1' then  -- rising clock edge
      fill_tag_dv <= '0';
      case state is
        when S_RESET =>
          ---S_RESET-------------------------------------------------------------
          fill_tag <= FIRST_VALID_TAG;
          if wr_rst_busy = '0' and rd_rst_busy = '0' then
            state <= S_FILL;
            fill_tag_dv <= '1';
          end if;
        when S_FILL =>
          ---S_FILL--------------------------------------------------------------
          if fill_tag /= LAST_VALID_TAG then
            fill_tag_dv <= '1';
            fill_tag <= fill_tag+1;
          else
            state <= S_RUNNING;
          end if;
        when S_RUNNING =>
          ---S_RUNNING-----------------------------------------------------------
         state <= S_RUNNING;
        when S_ERROR =>
          ---S_ERROR-------------------------------------------------------------
        when others =>
          ---others--------------------------------------------------------------
          state <= S_ERROR;
      end case;
    end if;
  end process reset_init_controller;

  --FIFO muxing
  fifo_wr_data <= std_logic_vector(fill_tag) when state = S_FILL else std_logic_vector(used_tag);
  fifo_wr_dv   <= fill_tag_dv                when state = S_FILL else used_tag_dv;

  --FIFO!
  free_tag <= Tag_t(free_tag_local);
  TAG_FIFO_1: entity work.TAG_FIFO
    port map (
      srst        => reset,
      wr_clk      => clk,
      rd_clk      => clk,
      din         => fifo_wr_data,
      wr_en       => fifo_wr_dv,
      rd_en       => free_tag_ack,
      dout        => free_tag_local,
      full        => open,
      empty       => fifo_low,
      valid       => free_tag_dv,
      wr_rst_busy => wr_rst_busy,
      rd_rst_busy => rd_rst_busy);
  
end architecture behavioral;
