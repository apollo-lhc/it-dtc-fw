library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity PriorityEncoder_4bit_LSB is
  port (
    input        : in  std_logic_vector(3 downto 0); 
    index        : out std_logic_vector(1 downto 0);
    valid        : out std_logic
    );
end entity PriorityEncoder_4bit_LSB;

architecture behavioral of PriorityEncoder_4bit_LSB is
    -- LUT documentation at: https://docs.xilinx.com/v/u/2014.1-English/ug974-vivado-ultrascale-libraries
    constant LUT_TABLE_4Channel : std_logic_vector(63 downto 0) := (
      --no free tags
      x"00" => '0', x"20" => '0',      x"10" => '0', x"30" => '0',
      -- input 0 active (given priority)
      x"01" => '0', x"21" => '0',      x"03" => '0', x"23" => '0',      x"05" => '0', x"25" => '0',      x"07" => '0', x"27" => '0',
      x"09" => '0', x"29" => '0',      x"0B" => '0', x"2B" => '0',      x"0D" => '0', x"2D" => '0',      x"0F" => '0', x"2F" => '0',
      x"11" => '0', x"31" => '0',      x"13" => '0', x"33" => '0',      x"15" => '0', x"35" => '0',      x"17" => '0', x"37" => '0',
      x"19" => '0', x"39" => '0',      x"1B" => '0', x"3B" => '0',      x"1D" => '0', x"3D" => '0',      x"1F" => '0', x"3F" => '0',
      -- input 0 '0'
      -- input 1 active (next priority)
      x"02" => '1', x"22" => '0',
      x"06" => '1', x"26" => '0',
      x"0A" => '1', x"2A" => '0',
      x"0E" => '1', x"2E" => '0',
      x"12" => '1', x"32" => '0',
      x"16" => '1', x"36" => '0',
      x"1A" => '1', x"3A" => '0',
      x"1E" => '1', x"3E" => '0',
      -- input 0,1 '0'
      -- input 2 active (next priority)
      x"04" => '0', x"24" => '1',
      x"0C" => '0', x"2C" => '1',
      x"14" => '0', x"34" => '1',
      x"1C" => '0', x"3C" => '1',
      -- input 0,1,2 '0'
      -- input 3 active (final priority)
      x"08" => '1', x"28" => '1',
      x"18" => '1', x"38" => '1'
      );
  
  
begin
  -- priority encoder
  LUT6_2_inst: LUT6_2
    generic map (
      INIT => LUT_TABLE_4Channel)
    );
  port map (
    I0 => input(0),
    I1 => input(1),
    I2 => input(2),
    I3 => input(3),
    O5 => index(0),
    O6 => index(1)
  );
  -- group of channels has active
  channel_active: process (input) is
  begin  -- process channel_active
    valid <= or_reduce(input);
  end process channel_active;

end architecture behavioral;
